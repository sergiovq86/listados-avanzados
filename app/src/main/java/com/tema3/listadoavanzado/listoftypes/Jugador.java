package com.tema3.listadoavanzado.listoftypes;

import android.os.Parcel;
import android.os.Parcelable;

public class Jugador implements Parcelable {

    private String apodo, nombre, posicion, fecha, lugar, procedencia, descripcion, foto, twitter;
    private int pos;
    private double altura, peso;

    public Jugador(String apodo, String nombre, String posicion, String fecha, String lugar,
                   double altura, double peso, String procedencia, String descripcion,
                   String foto, int pos, String twitter) {
        this.apodo = apodo;
        this.nombre = nombre;
        this.posicion = posicion;
        this.fecha = fecha;
        this.lugar = lugar;
        this.altura = altura;
        this.peso = peso;
        this.procedencia = procedencia;
        this.descripcion = descripcion;
        this.foto = foto;
        this.pos = pos;
        this.twitter = twitter;
    }

    protected Jugador(Parcel in) {
        apodo = in.readString();
        nombre = in.readString();
        posicion = in.readString();
        fecha = in.readString();
        lugar = in.readString();
        altura = in.readDouble();
        peso = in.readDouble();
        procedencia = in.readString();
        descripcion = in.readString();
        foto = in.readString();
        twitter = in.readString();
        pos = in.readInt();
    }

    public static final Creator<Jugador> CREATOR = new Creator<Jugador>() {
        @Override
        public Jugador createFromParcel(Parcel in) {
            return new Jugador(in);
        }

        @Override
        public Jugador[] newArray(int size) {
            return new Jugador[size];
        }
    };

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(apodo);
        dest.writeString(nombre);
        dest.writeString(posicion);
        dest.writeString(fecha);
        dest.writeString(lugar);
        dest.writeDouble(altura);
        dest.writeDouble(peso);
        dest.writeString(procedencia);
        dest.writeString(descripcion);
        dest.writeString(foto);
        dest.writeString(twitter);
        dest.writeInt(pos);
    }
}
