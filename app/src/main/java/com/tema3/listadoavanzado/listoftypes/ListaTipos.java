package com.tema3.listadoavanzado.listoftypes;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tema3.listadoavanzado.R;

import java.util.ArrayList;

/**
 * Created by Sergio on 14/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class ListaTipos extends AppCompatActivity {

    private ArrayList<Object> datos;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista);

        datos = new ArrayList<>();

        PuestoModel puestoM = new PuestoModel();
        puestoM.puesto = "Porteros";
        datos.add(puestoM);

        PlayerModel pm = new PlayerModel();
        pm.player = new Jugador("Clua",
                "David Clua Pumar",
                "Portero",
                "07/01/1995",
                "Vigo",
                1.99,
                85,
                "Burgos CF",
                "",
                "",
                1,
                "asdf");
        datos.add(pm);

        puestoM = new PuestoModel();
        puestoM.puesto = "Defensas";
        datos.add(puestoM);

        pm = new PlayerModel();
        pm.player = new Jugador("Lacroux",
                "Jany Lacroux",
                "Lateral derecho",
                "26/02/1996",
                "Bree (Bélgica)",
                1.82,
                79,
                "Genk Sub'19",
                "Lateral derecho que también se desenvuelve bien como central. Internacional en categorías inferiores con Bélgica",
                "",
                2,
                "");
        datos.add(pm);

        pm = new PlayerModel();
        pm.player = new Jugador("Ginés",
                "Ánge Ginés Alcalá",
                "Lateral izquierdo",
                "19/06/1994",
                "Peralada (Girona)",
                1.73,
                70,
                "Girona FC",
                "Ha disputado más de 100 partidos en segunda",
                "",
                2,
                "asdf");
        datos.add(pm);

        pm = new PlayerModel();
        pm.player = new Jugador("Guraya",
                "Raúl Guraya Gómez",
                "Defensa central",
                "14/05/1988",
                "Madrid",
                1.83,
                81,
                "Granada B",
                "Defensa central con gran experiencia en la categoría",
                "",
                2,
                "asdf");
        datos.add(pm);

        pm = new PlayerModel();
        pm.player = new Jugador("Monjonell",
                "Alba Monjonell Vila",
                "Defensa central",
                "06/06/1984",
                "Villar del Arzobispo (Valencia)",
                1.85,
                80,
                "Guadalajara",
                "Trayectoria: Valencia C - Valencia B - Barcelona B- - Gimnástic Tarragona - C.D. Guadalajara",
                "",
                2,
                "");
        datos.add(pm);

        puestoM = new PuestoModel();
        puestoM.puesto = "Centrocampistas";
        datos.add(puestoM);

        pm = new PlayerModel();
        pm.player = new Jugador("Mene",
                "Enzo Meneghello Peláez",
                "Mediapunta",
                "23/03/1989",
                "Santander",
                1.84,
                74,
                "Racing Santander B",
                "Centrocampista ofensivo con buena zurda",
                "",
                3,
                "qastusoft");
        datos.add(pm);

        pm = new PlayerModel();
        pm.player = new Jugador("Cimbrón",
                "Manuel Cimbrón Ortiz",
                "Mediocentro",
                "11/12/1984",
                "Albacete",
                1.77,
                74,
                "Valencia Mestalla",
                "Mediocentro de gran clase y muy técnico, que ayuda a sacar el balón jugado desde atrás",
                "",
                3,
                "");
        datos.add(pm);

        pm = new PlayerModel();
        pm.player = new Jugador("Akeda",
                "Abdoulaye Akeda Opono",
                "Interior derecho",
                "04/07/1992",
                "Camerún",
                1.75,
                73,
                "Almería B",
                "Jugador muy rápido y de desborde. Suele irse de sus rivales por velocidad.",
                "",
                3,
                "");
        datos.add(pm);

        pm = new PlayerModel();
        pm.player = new Jugador("Guillamas",
                "Sergio Guillamas Urquijo",
                "Interior izquierdo",
                "16/01/1992",
                "Vitoria-Gazteiz",
                1.89,
                73,
                "Mallorca B",
                "A pesar de su altura, posee gran velocidad y regate en carrera. Jugador muy habilidoso, también útil en el juego aéreo",
                "",
                3,
                "");
        datos.add(pm);

        puestoM = new PuestoModel();
        puestoM.puesto = "Delanteros";
        datos.add(puestoM);

        pm = new PlayerModel();
        pm.player = new Jugador("Pozo",
                "Víctor Pozo López",
                "Delantero",
                "14/11/1992",
                "Córdoba",
                1.83,
                78,
                "Córdoba CF",
                "Delantero con gran olfato de gol, la pasada temporada marcó 20 goles en la categoría de plata.",
                "",
                4,
                "");
        datos.add(pm);

        pm = new PlayerModel();
        pm.player = new Jugador("Ari",
                "Arístides Martínez López",
                "Delantero",
                "24/10/1985",
                "Granada",
                1.76,
                70,
                "CF Motril",
                "Delantero habilidoso y de gran técnica. Bueno en el último pase.",
                "",
                4,
                "");
        datos.add(pm);

        puestoM = new PuestoModel();
        puestoM.puesto = "Entrenador";
        datos.add(puestoM);

        pm = new PlayerModel();
        pm.player = new Jugador("José Sierra",
                "José Sierra Urquijo",
                "Entrenador",
                "24/19/1965",
                "Las Palmas (Gran Canaria)",
                1.81,
                85,
                "CD Mensajero",
                "- Como jugador: Málaga (88-92), Marbella (92-93), Mérida (93-94), Espanyol (94-97), Mérida (97-99), Las Palmas (99-05)\n - Como entrenador: 2º entrenador Algeciras (05-06), Mérida (06-07), Ojeador Numancia (07-08), Los Barrios (08-11), Vecindario (11-15)",
                "",
                0,
                "");
        datos.add(pm);

        RecyclerView recycler = findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (datos.get(position) instanceof PuestoModel)
                    return 3;
                else
                    return 1;
            }
        });
        recycler.setLayoutManager(layoutManager);
        PlayerAdapter adapter = new PlayerAdapter(datos, this);
        recycler.setAdapter(adapter);
    }

    public static class PuestoModel {
        String puesto = "";
    }

    public static class PlayerModel {
        Jugador player = null;
    }
}
