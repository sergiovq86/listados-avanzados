package com.tema3.listadoavanzado.listoftypes;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tema3.listadoavanzado.R;

import java.util.ArrayList;

/**
 * Created by Sergio on 15/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class PlayerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int PUESTO = 0, JUGADOR = 1;
    private final ArrayList<Object> datos;
    private final Context context;

    PlayerAdapter(ArrayList<Object> datos, Context context) {
        this.datos = datos;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(context);
        switch (viewType) {
            case PUESTO:
                View pView = inflater.inflate(R.layout.plantilla_title, parent, false);
                holder = new PuestoHolder(pView);
                break;

            case JUGADOR:
                View jview = inflater.inflate(R.layout.plantilla_holder, parent, false);
                holder = new PlayerHolder(jview);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case PUESTO:
                PuestoHolder pholder = (PuestoHolder) holder;
                ListaTipos.PuestoModel pm = (ListaTipos.PuestoModel) datos.get(position);
                String titulo = pm.puesto;
                pholder.setDia(titulo);
                break;

            case JUGADOR:
                PlayerHolder jholder = (PlayerHolder) holder;
                ListaTipos.PlayerModel jm = (ListaTipos.PlayerModel) datos.get(position);
                final Jugador thisplayer = jm.player;
                jholder.setText(thisplayer.getPosicion(), thisplayer.getApodo(), thisplayer.getFoto());
                jholder.ficha.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(context, DetalleJugador.class);
                        i.putExtra("objeto", thisplayer);
//                        Bundle b = new Bundle();
//                        b.putParcelable("objeto", thisplayer);
//                        i.putExtra("extra", b);
                        context.startActivity(i);
                    }
                });

        }
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (datos.get(position) instanceof ListaTipos.PuestoModel)
            return PUESTO;
        else
            return JUGADOR;

    }

    static class PuestoHolder extends RecyclerView.ViewHolder {
        private final TextView puesto;

        PuestoHolder(View view) {
            super(view);

            puesto = view.findViewById(R.id.title);
        }

        void setDia(String nombre) {
            puesto.setText(nombre);
        }
    }

    static class PlayerHolder extends RecyclerView.ViewHolder {
        private final TextView posicion;
        private final TextView apodo;
        private final ImageView foto;
        private final LinearLayout ficha;

        PlayerHolder(View view) {
            super(view);
            posicion = view.findViewById(R.id.posicion);
            apodo = view.findViewById(R.id.apodo);
            foto = view.findViewById(R.id.foto);
            ficha = view.findViewById(R.id.ficha);
        }

        void setText(String pos, String name, String pic) {
            posicion.setText(pos);
            apodo.setText(name);
            if (!pic.equals("")) {
                foto.setImageResource(Integer.parseInt(pic));
            }
        }
    }
}