package com.tema3.listadoavanzado.listoftypes;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tema3.listadoavanzado.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class DetalleJugador extends AppCompatActivity {

    private String apodo, nombre, posicion, fecha, lugar, altura, peso, procedencia, descripcion, foto, twitter;
    private int pos;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalle);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            Jugador jugador = b.getParcelable("objeto");
            apodo = jugador.getApodo();
            nombre = jugador.getNombre();
            posicion = jugador.getPosicion();
            fecha = getAge(jugador.getFecha());
            lugar = jugador.getLugar();
            altura = jugador.getAltura() + " m.";
            peso = jugador.getPeso() + " kg.";
            procedencia = jugador.getProcedencia();
            descripcion = jugador.getDescripcion();
            foto = jugador.getFoto();
            twitter = jugador.getTwitter();
            pos = jugador.getPos();

            setTitle(apodo);

            FloatingActionButton fab = findViewById(R.id.btnFab);
            if (twitter.equals(""))
                fab.setVisibility(View.GONE);
            else
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                    .parse("twitter://user?screen_name=" + twitter)));
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                    .parse("https://twitter.com/#!/" + twitter)));
                        }
                    }
                });

            rellenaDatos();
        }
    }

    private void rellenaDatos() {
        ArrayList<HashMap<String, String>> lista = new ArrayList<>();
        HashMap<String, String> info = new HashMap<>();

        info.put("title", getString(R.string.nombre));
        info.put("value", nombre);
        lista.add(info);
        info = new HashMap<>();

        if (pos == 0)
            info.put("title", getString(R.string.puesto));
        else
            info.put("title", getString(R.string.posicion));
        info.put("value", posicion);
        lista.add(info);
        info = new HashMap<>();

        info.put("title", getString(R.string.edad));
        info.put("value", fecha);
        lista.add(info);
        info = new HashMap<>();

        info.put("title", getString(R.string.nacimiento));
        info.put("value", lugar);
        lista.add(info);
        info = new HashMap<>();

        info.put("title", getString(R.string.altura));
        info.put("value", altura);
        lista.add(info);
        info = new HashMap<>();

        info.put("title", getString(R.string.peso));
        info.put("value", peso);
        lista.add(info);
        info = new HashMap<>();

        info.put("title", getString(R.string.procedencia));
        info.put("value", procedencia);
        lista.add(info);
        info = new HashMap<>();

        info.put("title", getString(R.string.descripcion));
        info.put("value", descripcion);
        lista.add(info);

        RecyclerView recycler = findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DetalleJugador.this);
        recycler.setLayoutManager(layoutManager);
        DataPlayerAdapter adapter = new DataPlayerAdapter(lista);
        recycler.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getAge(String birthday) {
        Calendar dob = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
        try {
            dob.setTime(sdf.parse(birthday));
            Calendar today = Calendar.getInstance();

            int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--;
            }

            return age + " años (" + birthday + ")";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return birthday;
    }
}
