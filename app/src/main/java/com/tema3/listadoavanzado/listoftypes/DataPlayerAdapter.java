package com.tema3.listadoavanzado.listoftypes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tema3.listadoavanzado.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sergio on 15/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class DataPlayerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<HashMap<String, String>> datos;

    public DataPlayerAdapter(ArrayList<HashMap<String, String>> datos) {
        this.datos = datos;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.detalle_jugador_holder, parent, false);
        return new JHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        JHolder celda = (JHolder) holder;
        HashMap<String, String> mapa = datos.get(position);
        String titulo = mapa.get("title");
        String valor = mapa.get("value");
        celda.title.setText(titulo);
        celda.value.setText(valor);
    }

    public static class JHolder extends RecyclerView.ViewHolder {
        TextView value, title;

        public JHolder(View view) {
            super(view);
            value = view.findViewById(R.id.value);
            title = view.findViewById(R.id.title);
        }
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }
}