package com.tema3.listadoavanzado.usingclasses;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tema3.listadoavanzado.R;

import java.util.ArrayList;

/**
 * Created by Sergio on 14/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class ListaClases extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista);

        ArrayList<Producto> product_array = new ArrayList<>();
        product_array.add(new Producto("Camiseta 1ª equipación",
                "35€",
                "camiseta",
                "Camiseta de la primera equipación con la que MiEquipo juega los partidos de casa",
                "Tallas: S, M, L y XL",
                ""));
        product_array.add(new Producto("Camiseta 2ª equipación",
                "35€",
                "camiseta2",
                "Camiseta de la segunda equipación con la que MiEquipo juega los partidos de fuera",
                "Tallas: M, L y XL",
                "30€"));
        product_array.add(new Producto("Equipación portero",
                "50€",
                "portero",
                "Equipación completa de portero",
                "Tallas: L y XL",
                ""));
        product_array.add(new Producto("Pantalón 1ª equipación",
                "15€",
                "pantalon",
                "Pantalón de la primera equipación de MiEquipo",
                "Tallas: XS, M, XL y XXL",
                ""));
        product_array.add(new Producto("Medias MiEquipo",
                "6€",
                "medias",
                "Medias de la primera equipación de MiEquipo",
                "Tallas: Desde el 40 hasta el 47",
                ""));
        product_array.add(new Producto("Chándal completo",
                "60€",
                "chandal",
                "Chándal completo de MiEquipo CF",
                "Tallas: M, L y XL",
                "50€"));
        product_array.add(new Producto("Chaqueta entrenamiento",
                "30€",
                "chaqueta",
                "Chaqueta entrenamiento de MiEquipo CF",
                "Tallas: M, L y XL",
                ""));
        product_array.add(new Producto("Polo oficial",
                "25€",
                "polo",
                "",
                "Tallas: M y L",
                "18€"));
        product_array.add(new Producto("Balón MiEquipo",
                "20€",
                "balon",
                "Balón con los colores y el escudo de MiEquipo",
                "",
                "15€"));
        product_array.add(new Producto("Bufanda MiEquipo",
                "8€",
                "bufanda",
                "Bufanda con los colores de MiEquipo CF",
                "Tamaño: 130cm x 17 cm",
                ""));
        product_array.add(new Producto(
                "Gorro MiEquipo",
                "12€",
                "sombrero",
                "Gorro con los colores y el escudo de MiEquipo",
                "",
                ""));
        product_array.add(new Producto("Taza MiEquipo",
                "9€",
                "taz",
                "Taza de MiEquipo CF",
                "8 cm de alto",
                ""));

        RecyclerView recycler = findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recycler.setLayoutManager(layoutManager);
        ProductAdapter adapter = new ProductAdapter(product_array, this);
        recycler.setAdapter(adapter);
    }
}
