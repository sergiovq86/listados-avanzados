package com.tema3.listadoavanzado.usingclasses;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tema3.listadoavanzado.R;

import java.util.ArrayList;

/**
 * Created by Sergio on 14/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<Producto> datos;
    private final Context context;

    ProductAdapter(ArrayList<Producto> datos, Context context) {
        this.datos = datos;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View pView = inflater.inflate(R.layout.producto_holder, parent, false);
        return new ProductHolder(pView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ProductHolder pholder = (ProductHolder) holder;
        final Producto este = datos.get(position);
        String titulo = este.getNombre();
        String precio = este.getPrecio();
        String imagen = este.getImagen();
        pholder.setText(titulo, precio, imagen, este.getRebaja());

        pholder.foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProductoDetalle.class);
                i.putExtra("objeto", este);
//                Bundle b = new Bundle();
//                b.putParcelable("objeto", este);
//                i.putExtra("extra", b);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }


    class ProductHolder extends RecyclerView.ViewHolder {
        private final TextView nombre;
        private final TextView precio;
        private final TextView antes;
        private final ImageView foto;
        private final View fondo;

        ProductHolder(View view) {
            super(view);
            foto = view.findViewById(R.id.imagen);
            nombre = view.findViewById(R.id.nombre);
            precio = view.findViewById(R.id.precio);
            fondo = view.findViewById(R.id.contenido);
            antes = view.findViewById(R.id.antes);
        }

        void setText(String name, String prize, String pic, String rebaja) {
            nombre.setText(name);

            int url = context.getResources().getIdentifier(pic, "drawable", context.getPackageName());
            foto.setImageResource(url);
            if (rebaja.equals("")) {
                antes.setText("");
                precio.setText(prize);
                fondo.setBackgroundColor(Color.parseColor("#bb888888"));
            } else {
                antes.setText(prize);
                antes.setPaintFlags(antes.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                precio.setText(rebaja);
                fondo.setBackgroundColor(Color.parseColor("#88880000"));
            }
        }
    }
}