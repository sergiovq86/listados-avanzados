package com.tema3.listadoavanzado.usingclasses;

import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.tema3.listadoavanzado.R;

public class ProductoDetalle extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.producto);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Bundle b = getIntent().getExtras();
        if (b != null) {
            Producto producto = b.getParcelable("objeto");
            TextView title = findViewById(R.id.titular);
            TextView precio = findViewById(R.id.precio);
            TextView tRebaja = findViewById(R.id.rebaja_title);
            TextView rebaja = findViewById(R.id.rebaja);
            TextView talla = findViewById(R.id.talla);
            ImageView foto = findViewById(R.id.imagen);
            TextView desc = findViewById(R.id.desc);

            setTitle(producto.getNombre());
            talla.setText(producto.getTalla());
            title.setText(producto.getNombre());
            desc.setText(producto.getDescripcion());
            int url = getResources().getIdentifier(producto.getImagen(), "drawable", getPackageName());
            foto.setImageResource(url);

            String antes = producto.getRebaja();
            String prize = producto.getPrecio();
            if (!antes.equals("")) {
                rebaja.setText(prize);
                rebaja.setPaintFlags(rebaja.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                precio.setText(antes);
            } else {
                precio.setText(prize);
                rebaja.setVisibility(View.GONE);
                tRebaja.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        try {
            if (item.getItemId() == android.R.id.home) {
                finish();
            }
        } catch (Exception ex) {
            Log.e("HomeButton ", ex.getMessage());
            ex.printStackTrace();
        }

        return super.onOptionsItemSelected(item);
    }
}
